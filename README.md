# GOSH 2022 

A place to store visual assets and graphic design files from the 2022 Gathering in Panama

## Brochure
- [PDF file](https://gitlab.com/gosh-community/gosh-2022/-/blob/main/GOSH_Brochure_v2.pdf)
- [.sla file](https://gitlab.com/gosh-community/gosh-2022/-/blob/main/GOSH_Brochure_v2__1_.sla)

